package com.homework.lib.repository;

import com.homework.lib.model.dto.UnderTest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnderTestRepository extends CrudRepository<UnderTest, Long> {
}
