package com.homework.lib.model.dto;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "underTest")
public class UnderTest {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
}
