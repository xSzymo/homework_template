package com.homework.lib.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "test")
public class Test {
    @Id
    @GeneratedValue
    private Long id;
    private String test;
    @OneToOne(cascade = CascadeType.ALL)
    private UnderTest underTest;
}

