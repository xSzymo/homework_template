package com.homework.lib.controller;

import com.homework.lib.model.dto.Test;
import com.homework.lib.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {
    @Autowired
    private TestService testService;

    @GetMapping
    public Test test(Test test) {
        return testService.create(test);
    }

    @PostMapping
    public Iterable<Test> find() {
        return testService.find();
    }
}
