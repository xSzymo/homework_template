package com.homework.lib.service;

import com.homework.lib.model.dto.Test;
import com.homework.lib.model.dto.UnderTest;
import com.homework.lib.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {
    @Autowired
    private TestRepository testRepository;

    public Test create(Test test) {
        Test build = new Test();
        build.setTest(test.getTest());
        build.setUnderTest(new UnderTest());
        return testRepository.save(build);
    }

    public Iterable<Test> find() {
        return testRepository.findAll();
    }
}
